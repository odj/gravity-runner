//
//  Settings.swift
//  GravityRunner
//
//  Created by David Emannuel Malkowski on 8/7/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

import Foundation

class Settings: CCNode {
    
    
    weak var musicButton: CCButton!
    weak var soundButton: CCButton!
    
    
    override func onEnter() {
        super.onEnter()
    }
    
    
    func soundEnabled () {
        
        if soundButton.selected == true {
            NSUserDefaults.standardUserDefaults().setBool(false, forKey:"soundEnabled")
            OALSimpleAudio.sharedInstance().effectsMuted = true
            NSUserDefaults.standardUserDefaults().synchronize()
        } else if soundButton.selected == false {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey:"soundEnabled")
            OALSimpleAudio.sharedInstance().effectsMuted = false
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    
    func musicEnabled () {
        print("music enabled pressed")
        if musicButton.selected == true {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey:"musicEnabled")
            OALSimpleAudio.sharedInstance().bgMuted = true
        } else if musicButton.selected == false {
            NSUserDefaults.standardUserDefaults().setBool(false, forKey:"soundEnabled")
            OALSimpleAudio.sharedInstance().bgMuted = false
        }
        
    }
    
    
    func didLoadFromCCB() {
        
        if NSUserDefaults.standardUserDefaults().boolForKey("musicEnabled") == true {
            musicButton.selected = false
        }
        
        if NSUserDefaults.standardUserDefaults().boolForKey("soundEnabled")  == true{
            soundButton.selected = false
        }

    }
    
    
    func backButton() {
        let scene = CCBReader.loadAsScene("MainMenu")
        CCDirector.sharedDirector().presentScene(scene)
    }
    
    
}