//
//  Obstacle.swift
//  GravityRunner
//
//  Created by David Emannuel Malkowski on 6/23/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

import Foundation

class Obstacle : CCNode {
    weak var topCrate : CCNode!
    weak var bottomCrate : CCNode!
    
    let topCrateMinimumPositionY : CGFloat = 128
    let bottomCrateMaximumPositionY : CGFloat = 440
    let crateDistance : CGFloat = 142
    
    func setupRandomPosition() {
        let randomPrecision : UInt32 = 100
        let random = CGFloat(arc4random_uniform(randomPrecision)) / CGFloat(randomPrecision)
        let range = bottomCrateMaximumPositionY - crateDistance - topCrateMinimumPositionY
        topCrate.position = ccp(topCrate.position.x, topCrateMinimumPositionY + (random * range));
        bottomCrate.position = ccp(bottomCrate.position.x, topCrate.position.y + crateDistance);
    }
}