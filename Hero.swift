//
//  Hero.swift
//  GravityRunner
//
//  Created by David Emannuel Malkowski on 7/23/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

import Foundation
class Hero: CCSprite {
    
    enum lineStat{
        case Above, Below
    }

    

    var heroJumpHeight: CGFloat = 600
    var heroScrollSpeed = 350
    var canJump: Bool = true
    var canFlip: Bool = true
    
    
    
    var lineSide: lineStat = .Above {
        didSet{
            if lineSide == .Above{
                flipY = false
                heroJumpHeight = 600
            } else {
                flipY = true
                heroJumpHeight = -600
            }
        }
    }
    
    func didLoadFromCCB(){
    }
    
    func jump() {
        if canJump {
            physicsBody.applyImpulse(ccp(0,heroJumpHeight))
        }
    }
    
    func flip() {
        if canFlip {
            
        }
        
    }
    
    

}