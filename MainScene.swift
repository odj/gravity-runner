import Foundation

class MainScene: CCNode, CCPhysicsCollisionDelegate {
    var highScore: Int = NSUserDefaults.standardUserDefaults().integerForKey("highScore") {
        didSet {
            NSUserDefaults.standardUserDefaults().setInteger(highScore, forKey: "highScore")
        }
    }
    
    weak var steve: Hero!
    weak var gamePhysicsNode : CCPhysicsNode!
    weak var line1 : CCNodeColor!
    weak var line2 : CCNodeColor!
    weak var obstaclesLayer : CCNode!
    weak var pauseModal : CCSprite!
    weak var pauseButton : CCButton!
    weak var restartButton : CCButton!
    weak var mainMenuButton : CCButton!
    weak var distanceLabel : CCLabelTTF!
    weak var distanceHeader : CCLabelTTF!
    weak var highScoreLabel : CCLabelTTF!
    weak var highScoreText : CCLabelTTF!
    weak var reportCard : CCLabelTTF!
    weak var pauseLabel : CCLabelTTF!
    let screenSize: CGRect = UIScreen.mainScreen().bounds
    let firstObstaclePosition : CGFloat = 280
    let distanceBetweenObstacles : CGFloat = 200
    var lines = [CCNodeColor]()  // initializes an empty array
    var sinceTouch : CCTime = 0
    var scrollSpeed : CGFloat = 300
    var gameOver = false
    var obstacles : [Obstacle] = []
    var distance : Double = 0{
        didSet{
            if Int(distance) > highScore {
                highScore = Int(distance)
            }
            distanceLabel.string = "\(Int(distance))"
            highScoreLabel.string = "\(Int(highScore))"
        }
    }
    
    
    func didLoadFromCCB() {
        
        var highScore: Int = NSUserDefaults.standardUserDefaults().integerForKey("highScore") {
            didSet {
                NSUserDefaults.standardUserDefaults().setInteger(highScore, forKey: "highScore")
            }
        }

        if NSUserDefaults.standardUserDefaults().boolForKey("musicEnabled") == true {
        let song = arc4random_uniform(2) == 0 ? "NanoHoedown.mp3" : "WagonWheel-Electronic.mp3"
            OALSimpleAudio.sharedInstance().playBg(song,loop:true)
        }
        
        print(NSUserDefaults.standardUserDefaults().integerForKey("highScore"))
        

        scheduleOnce("distance", delay: 0.05)
        gamePhysicsNode.debugDraw = false
        userInteractionEnabled = true
        lines.append(line1)
        lines.append(line2)
        gamePhysicsNode.collisionDelegate = self
        spawnNewObstacle()
        for _ in 0...2 {
            spawnNewObstacle()
        }
        
        schedule("spawnNewObstacle", interval: 0.5)
        setupGestures()
    }
    
    
    override func touchBegan(touch: CCTouch!, withEvent event: CCTouchEvent!){
//        scheduleOnce("actuallyJump", delay: 0.05)
        actuallyJump()
    }
    
    
    func actuallyJump() {
        if (!gameOver){
            steve.animationManager.runAnimationsForSequenceNamed("Jump")
            steve.physicsBody.allowsRotation = false
            steve.jump()
            steve.canJump = false
            sinceTouch = 0
        }
    }
    
    
    override func update(delta: CCTime) {
        
        if !gameOver{
            distance = distance + 0.1
        }
        
        //        let velocityY = clampf(Float(steve.physicsBody.velocity.y), -Float(CGFloat.max), 200)
        //        steve.physicsBody.velocity = ccp(0, CGFloat(velocityY))
        
        steve.position = ccp(steve.position.x + scrollSpeed * CGFloat(delta), steve.position.y)
        gamePhysicsNode.position = ccp(gamePhysicsNode.position.x - scrollSpeed * CGFloat(delta), gamePhysicsNode.position.y)
        
        
        
        for ground in lines {
            let groundWorldPosition = gamePhysicsNode.convertToWorldSpace(ground.positionInPoints)
            let groundScreenPosition = convertToNodeSpace(groundWorldPosition)
            
            if groundScreenPosition.x <= (-ground.contentSizeInPoints.width) {
                ground.positionInPoints = ccp(ground.positionInPoints.x + ground.contentSizeInPoints.width * 2, ground.positionInPoints.y)
            }
            
        }
        
        
        
        for obstacle in obstacles.reverse() {
            
            let obstacleWorldPosition = gamePhysicsNode.convertToWorldSpace(obstacle.position)
            let obstacleScreenPosition = convertToNodeSpace(obstacleWorldPosition)
            
            // obstacle moved past left side of screen?
            if obstacleScreenPosition.x < (-obstacle.contentSize.width) {
                obstacle.removeFromParent()
                obstacles.indexOf(obstacle)!
                
                // for each removed obstacle, add a new one
                
            }
        }
        
    }
    
    
    func setupGestures() {
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: "swipeUp")
        swipeUp.direction = .Up
        CCDirector.sharedDirector().view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: "swipeDown")
        swipeDown.direction = .Down
        CCDirector.sharedDirector().view.addGestureRecognizer(swipeDown)
        
    }
    
    
    func swipeUp() {
        if !gameOver && steve.canFlip{
            unschedule("actuallyJump")
            steve.lineSide = .Above
            gamePhysicsNode.gravity = ccp(0,-1000)
            steve.positionInPoints = ccp(steve.positionInPoints.x,screenSize.height * 0.60)
            print("\(steve.positionInPoints.x)")
            print("Up swipe!")
        }
    }
    
    
    func swipeDown() {
        if !gameOver && steve.canFlip{
            unschedule("actuallyJump")
            steve.lineSide = .Below
            gamePhysicsNode.gravity = ccp(0,1000)
            steve.positionInPoints = ccp(steve.positionInPoints.x,screenSize.height * 0.40)
            print("\(steve.positionInPoints.x)")
            print("Down swipe!")
        }
    }
    
    
    func spawnNewObstacle() {
        var prevObstaclePos = firstObstaclePosition
        if obstacles.count > 0 {
            prevObstaclePos = obstacles.last!.position.x
        }
        
        let obstacle = CCBReader.load("Obstacle") as! Obstacle
        obstacle.position = ccp(prevObstaclePos + distanceBetweenObstacles, 0)
        obstacle.setupRandomPosition()
        obstaclesLayer.addChild(obstacle)
        obstacles.append(obstacle)
    }
    
    
    func ccPhysicsCollisionBegin(pair: CCPhysicsCollisionPair!, hero: CCNode!, level: CCNode!) -> Bool {
        triggerGameOver()
        return true
    }
    
    
    func ccPhysicsCollisionBegin(pair: CCPhysicsCollisionPair!, hero: CCNode!, line: CCNode!) -> Bool {
        steve.canJump = true
        steve.canFlip = true
        if !gameOver {
            steve.animationManager.runAnimationsForSequenceNamed("jumpLanded")
        }
    return true
    }
    
    
    func ccPhysicsCollisionBegin(pair: CCPhysicsCollisionPair!, hero: CCNode!, box: CCNode!) -> Bool {
        steve.canJump = true
        return true
    }

    
    func ccPhysicsCollisionSeparate(pair: CCPhysicsCollisionPair!, hero: CCNode!, line: CCNode!) {
        steve.canFlip = true
    }
    
    
    func restart() {
        let scene = CCBReader.loadAsScene("MainScene")
        CCDirector.sharedDirector().presentScene(scene)
    }
    
    
    func mainMenuReturn() {
        let scene = CCBReader.loadAsScene("MainMenu")
        CCDirector.sharedDirector().presentScene(scene)
    }
    
    
    func triggerGameOver() {
        if (gameOver == false) {
            pauseButton.visible = false
            pauseModal.visible = true
            reportCard.visible = true
            highScoreLabel.visible = true
            highScoreText.visible = true
            distanceLabel.visible = false
            distanceHeader.visible = false
            
            steve.animationManager.runAnimationsForSequenceNamed("Dead")
            if NSUserDefaults.standardUserDefaults().boolForKey("musicEnabled") == true {
            OALSimpleAudio.sharedInstance().playBg("WinnerWinner.mp3",loop:true)
            }
            if NSUserDefaults.standardUserDefaults().boolForKey("soundEnabled") == true {
                OALSimpleAudio.sharedInstance().playEffect("sfx_hit.mp3")
            }
            

            gameOver = true
            restartButton.visible = true
            mainMenuButton.visible = true
            scrollSpeed = 0
            steve.rotation = 90
            steve.physicsBody.allowsRotation = false
            
            
            
            let move = CCActionEaseBounceOut(action: CCActionMoveBy(duration: 0.2, position: ccp(0, 4)))
            let moveBack = CCActionEaseBounceOut(action: move.reverse())
            let shakeSequence = CCActionSequence(array: [move, moveBack])
            runAction(shakeSequence)
        }
    }
    
    
    func pause(){
        if pauseButton.selected {
            self.paused = true
            pauseLabel.visible = true
            pauseModal.visible = true
            restartButton.visible = true
            mainMenuButton.visible = true
            let pauseSong = arc4random_uniform(2) == 0 ? "QuirkyDog.mp3" : "MonkeysSpinningMonkeys.mp3"
            OALSimpleAudio.sharedInstance().playBg(pauseSong,loop:true)
        } else {
            self.paused = false
            pauseLabel.visible = false
            pauseModal.visible = false
            restartButton.visible = false
            mainMenuButton.visible = false
            let song = arc4random_uniform(2) == 0 ? "NanoHoedown.mp3" : "WagonWheel-Electronic.mp3"
            OALSimpleAudio.sharedInstance().playBg(song,loop:true)
        }
    }
    
    
    
}