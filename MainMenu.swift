//
//  MainMenu.swift
//  GravityRunner
//
//  Created by David Emannuel Malkowski on 7/28/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

import Foundation

class MainMenu: CCNode {
    
  
    weak var infoButton: CCButton!
    weak var infoModal: CCNode!
    
   
    func didLoadFromCCB() {
//        
//        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "musicEnabled")
//        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "soundEnabled")
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "hasPlayedBefore")
        
        
        
        if NSUserDefaults.standardUserDefaults().boolForKey("hasPlayedBefore") == false {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "hasPlayedBefore")
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "musicEnabled")
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "soundEnabled")
        }
        

    
        if NSUserDefaults.standardUserDefaults().boolForKey("musicEnabled") == true {
            OALSimpleAudio.sharedInstance().playBg("Electrodoodle.mp3",loop:true)
        }
        
    }
    
    func settings() {
        let scene = CCBReader.loadAsScene("Settings")
        CCDirector.sharedDirector().presentScene(scene)
    }
    
    func restart() {
        print("restart was pressed")
        let scene = CCBReader.loadAsScene("MainScene")
        CCDirector.sharedDirector().presentScene(scene)
    }
    
    func showInfoModal() {
        if infoButton.selected == true {
            infoModal.visible = true
        } else if infoButton.selected == false {
            infoModal.visible = false
        }
    }
    
}